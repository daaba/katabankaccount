package fr.bankaccountmanagement.service;

import fr.bankaccountmanagement.exceptions.InvalidAmountException;
import fr.bankaccountmanagement.tools.NumberTools;
import fr.bankaccountmanagement.transverse.Account;
import fr.bankaccountmanagement.transverse.TransactionType;

import java.io.PrintStream;
import java.math.BigDecimal;
import java.time.LocalDateTime;

public class BankAccountService {

    private BankAccountService() {
        //
    }

    /**
     * Updates the balance of the account after applying the operation on the operation date
     *
     * @param account       bank account
     * @param amount        operation amount
     * @param operation     transaction type
     * @param operationDate operation date
     * @return updated account balance
     * @throws InvalidAmountException if amount <= 0
     */
    public static BigDecimal updateAccountBalance(Account account, BigDecimal amount, TransactionType operation,
                                                  LocalDateTime operationDate) throws InvalidAmountException {
        checkTransactionValidity(amount);
        return account.updateBalance(amount, operation, operationDate);
    }

    /**
     * Prints the account transactions on a printStream
     *
     * @param printStream stream to print transactions on
     * @param account     bank account
     * @return the printed transactions
     */
    public static String printAccountTransactions(PrintStream printStream, Account account) {
        String message = account.buildMessage();
        printStream.println(message);
        return message;
    }

    private static void checkTransactionValidity(BigDecimal amount) throws InvalidAmountException {
        NumberTools.checkInvalidAmount(amount);
    }
}
