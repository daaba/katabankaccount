package fr.katas.bankaccountmanagement.tools;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static fr.bankaccountmanagement.tools.DateTools.formatDate;

class DateToolsTest {

    @Test
    void formatDateNotNull() {
        Assertions.assertEquals("01/03/2019", formatDate(LocalDateTime.of(2019, 3, 1, 1, 50)));
    }

    @Test
    void formatDateNull() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> formatDate(null));
    }
}
