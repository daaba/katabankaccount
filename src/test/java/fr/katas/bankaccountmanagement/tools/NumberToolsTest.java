package fr.katas.bankaccountmanagement.tools;

import fr.bankaccountmanagement.exceptions.InvalidAmountException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static fr.bankaccountmanagement.tools.NumberTools.checkInvalidAmount;
import static fr.bankaccountmanagement.tools.NumberTools.format;

class NumberToolsTest {

    @Test
    void controlPermittedAmountTest() {
        try {
            checkInvalidAmount(BigDecimal.valueOf(10));
        } catch (Exception e) {
            Assertions.fail();
        }
    }

    @Test
    void controlZeroAmountTest() {
        Assertions.assertThrows(InvalidAmountException.class, () -> checkInvalidAmount(BigDecimal.ZERO));
    }


    @Test
    void checkAmountNull() {
        Assertions.assertThrows(InvalidAmountException.class, () -> checkInvalidAmount(null));
    }

    @Test
    void checkNegativeAmount() {
        try {
            checkInvalidAmount(BigDecimal.valueOf(-10));
            Assertions.fail();
        } catch (Exception e) {
            Assertions.assertEquals("Not permitted amount", e.getMessage());
        }
    }

    @Test
    void formatZero() {
        Assertions.assertEquals("0.00", format(BigDecimal.ZERO).toString());
    }

    @Test
    void formatSupThanHalf() {
        Assertions.assertEquals("2.49", format(BigDecimal.valueOf(2.487d)).toString());
    }

    @Test
    void formatInfThanHalf() {
        Assertions.assertEquals("2.48", format(BigDecimal.valueOf(2.483d)).toString());
    }

    @Test
    void formatHalfOddNumber() {
        Assertions.assertEquals("2.42", format(BigDecimal.valueOf(2.415d)).toString());
    }

    @Test
    void formatHalEvenNumber() {
        Assertions.assertEquals("2.46", format(BigDecimal.valueOf(2.465d)).toString());
    }
}
