package fr.katas.bankaccountmanagement.service;

import fr.bankaccountmanagement.exceptions.InvalidAmountException;
import fr.bankaccountmanagement.transverse.Account;
import fr.bankaccountmanagement.transverse.TransactionType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import static fr.bankaccountmanagement.service.BankAccountService.printAccountTransactions;
import static fr.bankaccountmanagement.service.BankAccountService.updateAccountBalance;

class BankAccountServiceTest {

    @Test
    void depositOnAccount() throws InvalidAmountException {
        Assertions.assertEquals("10.00",
                updateAccountBalance(new Account(), BigDecimal.valueOf(10d), TransactionType.DEPOSIT, LocalDateTime.now())
                        .toString());
    }

    @Test
    void depositBigAmountOnAccount() throws InvalidAmountException {
        Assertions.assertEquals("9999999999.99", updateAccountBalance(new Account(), BigDecimal.valueOf(9999999999.99d),
                TransactionType.DEPOSIT, LocalDateTime.now()).toString());
    }

    @Test
    void depositNotRoundedAmountOnAccount() throws InvalidAmountException {
        Assertions.assertEquals("0.01",
                updateAccountBalance(new Account(), BigDecimal.valueOf(0.01d), TransactionType.DEPOSIT, LocalDateTime.now())
                        .toString());
    }

    @Test
    void depositNotScaledAmountOnAccount() throws InvalidAmountException {
        Assertions.assertEquals("10.02", updateAccountBalance(new Account(), BigDecimal.valueOf(10.0190d),
                TransactionType.DEPOSIT, LocalDateTime.now()).toString());
    }

    @Test
    void makeMultipleDepositsOnAccount() throws InvalidAmountException {
        Account account = new Account();
        updateAccountBalance(account, BigDecimal.valueOf(28.90d), TransactionType.DEPOSIT, LocalDateTime.now());
        Assertions.assertEquals("58.90",
                updateAccountBalance(account, BigDecimal.valueOf(30.00d), TransactionType.DEPOSIT, LocalDateTime.now())
                        .toString());
    }

    @Test
    void makeRoundedWithdrawalFromOverdraftAccount() throws InvalidAmountException {
        Assertions.assertEquals("-30.00", updateAccountBalance(new Account(), BigDecimal.valueOf(30.00d),
                TransactionType.WITHDRAWAL, LocalDateTime.now()).toString());
    }

    @Test
    void withdrawalNotRoundedAmountFromAccount() throws InvalidAmountException {
        Assertions.assertEquals("-0.21", updateAccountBalance(new Account(), BigDecimal.valueOf(0.21d),
                TransactionType.WITHDRAWAL, LocalDateTime.now()).toString());
    }

    @Test
    void withdrawalNotScaledAmountFromAccount() throws InvalidAmountException {
        Assertions.assertEquals("-87.10", updateAccountBalance(new Account(), BigDecimal.valueOf(87.0966d),
                TransactionType.WITHDRAWAL, LocalDateTime.now()).toString());
    }

    @Test
    void withdrawalBigAmountOnAccount() throws InvalidAmountException {
        Assertions.assertEquals("-9999999999.19", updateAccountBalance(new Account(), BigDecimal.valueOf(9999999999.19d),
                TransactionType.WITHDRAWAL, LocalDateTime.now()).toString());
    }

    @Test
    void withdrawalMultipleAmountsFromAccount() throws InvalidAmountException {
        Account account = new Account();
        updateAccountBalance(account, BigDecimal.valueOf(10.10d), TransactionType.WITHDRAWAL, LocalDateTime.now());
        Assertions.assertEquals("-42.12",
                updateAccountBalance(account, BigDecimal.valueOf(32.02d), TransactionType.WITHDRAWAL, LocalDateTime.now())
                        .toString());
    }

    @Test
    void withdrawalThenDepositOnAccount() throws InvalidAmountException {
        Account account = new Account();
        updateAccountBalance(account, BigDecimal.valueOf(15.20d), TransactionType.WITHDRAWAL, LocalDateTime.now());
        Assertions.assertEquals("34.80",
                updateAccountBalance(account, BigDecimal.valueOf(50.00d), TransactionType.DEPOSIT, LocalDateTime.now())
                        .toString());
    }

    @Test
    void depositThenWithdrawalFromAccount() throws InvalidAmountException {
        Account account = new Account();
        updateAccountBalance(account, BigDecimal.valueOf(150.10d), TransactionType.DEPOSIT, LocalDateTime.now());
        Assertions.assertEquals("-15.40",
                updateAccountBalance(account, BigDecimal.valueOf(165.50d), TransactionType.WITHDRAWAL, LocalDateTime.now())
                        .toString());
    }

    @Test
    void withdrawalCompensateDepositOnAccount() throws InvalidAmountException {
        Account account = new Account();
        updateAccountBalance(account, BigDecimal.valueOf(15.20d), TransactionType.WITHDRAWAL, LocalDateTime.now());
        Assertions.assertEquals("0.00",
                updateAccountBalance(account, BigDecimal.valueOf(15.20d), TransactionType.DEPOSIT, LocalDateTime.now())
                        .toString());
    }

    @Test
    void depositCompensateWithdrawalOnAccount() throws InvalidAmountException {
        Account account = new Account();
        updateAccountBalance(account, BigDecimal.valueOf(50.00d), TransactionType.DEPOSIT, LocalDateTime.now());
        Assertions.assertEquals("0.00",
                updateAccountBalance(account, BigDecimal.valueOf(50.00d), TransactionType.WITHDRAWAL, LocalDateTime.now())
                        .toString());
    }

    @Test
    void makeMultipleDepositsThenWithdrawalsFromAccount() throws InvalidAmountException {
        Account account = new Account();
        updateAccountBalance(account, BigDecimal.valueOf(10.10d), TransactionType.DEPOSIT, LocalDateTime.now());
        updateAccountBalance(account, BigDecimal.valueOf(51.30d), TransactionType.DEPOSIT, LocalDateTime.now());
        updateAccountBalance(account, BigDecimal.valueOf(7.80d), TransactionType.DEPOSIT, LocalDateTime.now());
        updateAccountBalance(account, BigDecimal.valueOf(60.00d), TransactionType.WITHDRAWAL, LocalDateTime.now());
        updateAccountBalance(account, BigDecimal.valueOf(10.65d), TransactionType.WITHDRAWAL, LocalDateTime.now());
        Assertions.assertEquals("-71.46",
                updateAccountBalance(account, BigDecimal.valueOf(70.01d), TransactionType.WITHDRAWAL, LocalDateTime.now())
                        .toString());
    }

    @Test
    void makeMultipleWithdrawalsThenDepositsOnAccount() throws InvalidAmountException {
        Account account = new Account();
        updateAccountBalance(account, BigDecimal.valueOf(60.00d), TransactionType.WITHDRAWAL, LocalDateTime.now());
        updateAccountBalance(account, BigDecimal.valueOf(10.65d), TransactionType.WITHDRAWAL, LocalDateTime.now());
        updateAccountBalance(account, BigDecimal.valueOf(10.10d), TransactionType.DEPOSIT, LocalDateTime.now());
        updateAccountBalance(account, BigDecimal.valueOf(51.30d), TransactionType.DEPOSIT, LocalDateTime.now());
        updateAccountBalance(account, BigDecimal.valueOf(7.80d), TransactionType.DEPOSIT, LocalDateTime.now());
        Assertions.assertEquals("-71.46",
                updateAccountBalance(account, BigDecimal.valueOf(70.01d), TransactionType.WITHDRAWAL, LocalDateTime.now())
                        .toString());
    }

    @Test
    void makeMultipleWithdrawalsAndDepositsInMixedOrder() throws InvalidAmountException {
        Account account = new Account();
        updateAccountBalance(account, BigDecimal.valueOf(10.10d), TransactionType.DEPOSIT, LocalDateTime.now());
        updateAccountBalance(account, BigDecimal.valueOf(51.30d), TransactionType.DEPOSIT, LocalDateTime.now());
        updateAccountBalance(account, BigDecimal.valueOf(60.00d), TransactionType.WITHDRAWAL, LocalDateTime.now());
        updateAccountBalance(account, BigDecimal.valueOf(7.80d), TransactionType.DEPOSIT, LocalDateTime.now());
        updateAccountBalance(account, BigDecimal.valueOf(10.65d), TransactionType.WITHDRAWAL, LocalDateTime.now());
        Assertions.assertEquals("-71.46",
                updateAccountBalance(account, BigDecimal.valueOf(70.01d), TransactionType.WITHDRAWAL, LocalDateTime.now())
                        .toString());
    }

    @Test
    void makeNegativeDepositOnAccount() {
        Assertions.assertThrows(InvalidAmountException.class, () ->
                updateAccountBalance(new Account(), BigDecimal.valueOf(-10.70d), TransactionType.DEPOSIT, LocalDateTime.now()));
    }

    @Test
    void makeFakeDepositWithZeroAmount() {
        Assertions.assertThrows(InvalidAmountException.class, () -> updateAccountBalance(new Account(), BigDecimal.ZERO, TransactionType.DEPOSIT, LocalDateTime.now()));
    }

    @Test
    void makeFakeDepositWithNullAmount() {
        Assertions.assertThrows(InvalidAmountException.class, () -> updateAccountBalance(new Account(), null, TransactionType.DEPOSIT, LocalDateTime.now()));
    }

    @Test
    void makeNegativeWithdrawalFromAccount() {
        Assertions.assertThrows(InvalidAmountException.class, () -> updateAccountBalance(new Account(), BigDecimal.valueOf(-20.00d), TransactionType.WITHDRAWAL, LocalDateTime.now()));
    }

    @Test
    void makeFakeWithdrawalWithZeroAmount() {
        Assertions.assertThrows(InvalidAmountException.class, () -> updateAccountBalance(new Account(), BigDecimal.ZERO, TransactionType.WITHDRAWAL, LocalDateTime.now()));
    }

    @Test
    void makeFakeWithdrawalWithNullAmount() {
        Assertions.assertThrows(InvalidAmountException.class, () -> updateAccountBalance(new Account(), null, TransactionType.WITHDRAWAL, LocalDateTime.now()));
    }

    @Test
    void makeInvalidOperation() {
        Assertions.assertThrows(IllegalArgumentException.class, () ->
                updateAccountBalance(new Account(), BigDecimal.valueOf(10), TransactionType.valueOf("NOT_PERMITTED_OP"),
                        LocalDateTime.now()));
    }

    @Test
    void printOneDeposit() throws InvalidAmountException {
        Account account = new Account();
        LocalDateTime date = LocalDateTime.of(2019, 4, 1, 0, 0);
        updateAccountBalance(account, BigDecimal.valueOf(20.00d), TransactionType.DEPOSIT, date);
        Assertions.assertEquals("Deposit, the 01/04/2019 of 20.00 euros. Your account balance is of 20.00 euros",
                printAccountTransactions(System.out, account));
    }

    @Test
    void printMultipleDeposits() throws InvalidAmountException {
        LocalDateTime date = LocalDateTime.of(2019, 4, 3, 0, 0);
        Account account = new Account();
        updateAccountBalance(account, BigDecimal.valueOf(5.90d), TransactionType.DEPOSIT, date);
        updateAccountBalance(account, BigDecimal.valueOf(15.90d), TransactionType.DEPOSIT, date);
        Assertions.assertEquals(
                "Deposit, the 03/04/2019 of 5.90 euros. Your account balance is of 5.90 euros\n" +
                        "Deposit, the 03/04/2019 of 15.90 euros. Your account balance is of 21.80 euros",
                printAccountTransactions(System.out, account));
    }

    @Test
    void printOneWithdrawal() throws InvalidAmountException {
        Account account = new Account();
        updateAccountBalance(account, BigDecimal.valueOf(10.00d), TransactionType.WITHDRAWAL, LocalDateTime.of(2019, 2, 1, 0, 0));
        Assertions.assertEquals("Withdrawal, the 01/02/2019 of 10.00 euros. Your account balance is of -10.00 euros",
                printAccountTransactions(System.out, account));
    }

    @Test
    void printMultipleWithdrawals() throws InvalidAmountException {
        Account account = new Account();
        LocalDateTime date = LocalDateTime.of(2019, 4, 6, 0, 0);
        updateAccountBalance(account, BigDecimal.valueOf(10.20d), TransactionType.WITHDRAWAL, date);
        updateAccountBalance(account, BigDecimal.valueOf(35.01d), TransactionType.WITHDRAWAL, date);
        Assertions.assertEquals(
                "Withdrawal, the 06/04/2019 of 10.20 euros. Your account balance is of -10.20 euros\n"
                        + "Withdrawal, the 06/04/2019 of 35.01 euros. Your account balance is of -45.21 euros",
                printAccountTransactions(System.out, account));
    }

    @Test
    void printTransactionsOnEmptyAccount() {
        Assertions.assertEquals("No transactions recorded on empty account. Your balance is of 0 euros",
                printAccountTransactions(System.out, new Account()));
    }

    @Test
    void printDepositThenWithdrawalTransactions() throws InvalidAmountException {
        LocalDateTime date = LocalDateTime.of(2019, 4, 1, 0, 0);
        Account account = new Account();
        updateAccountBalance(account, BigDecimal.valueOf(100.00d), TransactionType.DEPOSIT, date);
        updateAccountBalance(account, BigDecimal.valueOf(5.87d), TransactionType.DEPOSIT, date);
        updateAccountBalance(account, BigDecimal.valueOf(3.5d), TransactionType.WITHDRAWAL, date);
        updateAccountBalance(account, BigDecimal.valueOf(85.10d), TransactionType.WITHDRAWAL, date);
        updateAccountBalance(account, BigDecimal.valueOf(0.01d), TransactionType.WITHDRAWAL, date);
        Assertions.assertEquals(
                "Deposit, the 01/04/2019 of 100.00 euros. Your account balance is of 100.00 euros\n"
                        + "Deposit, the 01/04/2019 of 5.87 euros. Your account balance is of 105.87 euros\n"
                        + "Withdrawal, the 01/04/2019 of 3.50 euros. Your account balance is of 102.37 euros\n"
                        + "Withdrawal, the 01/04/2019 of 85.10 euros. Your account balance is of 17.27 euros\n"
                        + "Withdrawal, the 01/04/2019 of 0.01 euros. Your account balance is of 17.26 euros",
                printAccountTransactions(System.out, account));
    }

    @Test
    void printWithdrawalThenDepositTransactions() throws InvalidAmountException {
        LocalDateTime date = LocalDateTime.of(2019, 4, 1, 0, 0);
        Account account = new Account();
        updateAccountBalance(account, BigDecimal.valueOf(35.90d), TransactionType.WITHDRAWAL, date);
        updateAccountBalance(account, BigDecimal.valueOf(5.91d), TransactionType.WITHDRAWAL, date);
        updateAccountBalance(account, BigDecimal.valueOf(0.20d), TransactionType.WITHDRAWAL, date);
        updateAccountBalance(account, BigDecimal.valueOf(18.10d), TransactionType.DEPOSIT, date);
        updateAccountBalance(account, BigDecimal.valueOf(1.15d), TransactionType.DEPOSIT, date);
        Assertions.assertEquals(
                "Withdrawal, the 01/04/2019 of 35.90 euros. Your account balance is of -35.90 euros\n"
                        + "Withdrawal, the 01/04/2019 of 5.91 euros. Your account balance is of -41.81 euros\n"
                        + "Withdrawal, the 01/04/2019 of 0.20 euros. Your account balance is of -42.01 euros\n"
                        + "Deposit, the 01/04/2019 of 18.10 euros. Your account balance is of -23.91 euros\n"
                        + "Deposit, the 01/04/2019 of 1.15 euros. Your account balance is of -22.76 euros",
                printAccountTransactions(System.out, account));
    }

    @Test
    void printMultipleWithdrawalsAndDeposalsInMixedOrder() throws InvalidAmountException {
        LocalDateTime date = LocalDateTime.of(2019, 4, 1, 0, 0);
        Account account = new Account();
        updateAccountBalance(account, BigDecimal.valueOf(20.00d), TransactionType.WITHDRAWAL, date);
        updateAccountBalance(account, BigDecimal.valueOf(20.00d), TransactionType.DEPOSIT, date);
        updateAccountBalance(account, BigDecimal.valueOf(5.00d), TransactionType.WITHDRAWAL, date);
        updateAccountBalance(account, BigDecimal.valueOf(100.00d), TransactionType.DEPOSIT, date);
        updateAccountBalance(account, BigDecimal.valueOf(35.00d), TransactionType.WITHDRAWAL, date);
        Assertions.assertEquals(
                "Withdrawal, the 01/04/2019 of 20.00 euros. Your account balance is of -20.00 euros\n"
                        + "Deposit, the 01/04/2019 of 20.00 euros. Your account balance is of 0.00 euros\n"
                        + "Withdrawal, the 01/04/2019 of 5.00 euros. Your account balance is of -5.00 euros\n"
                        + "Deposit, the 01/04/2019 of 100.00 euros. Your account balance is of 95.00 euros\n"
                        + "Withdrawal, the 01/04/2019 of 35.00 euros. Your account balance is of 60.00 euros",
                printAccountTransactions(System.out, account));
    }

    @Test
    void printMultipleWithdrawalsAndDeposalsMadeOnDifferentDates() throws InvalidAmountException {
        Account account = new Account();
        updateAccountBalance(account, BigDecimal.valueOf(20.00d), TransactionType.WITHDRAWAL,
                LocalDateTime.of(2019, 2, 1, 0, 0));
        updateAccountBalance(account, BigDecimal.valueOf(20.00d), TransactionType.DEPOSIT,
                LocalDateTime.of(2019, 2, 2, 0, 0));
        updateAccountBalance(account, BigDecimal.valueOf(5.00d), TransactionType.WITHDRAWAL,
                LocalDateTime.of(2019, 3, 4, 0, 0));
        updateAccountBalance(account, BigDecimal.valueOf(100.00d), TransactionType.DEPOSIT,
                LocalDateTime.of(2019, 4, 6, 0, 0));
        updateAccountBalance(account, BigDecimal.valueOf(35.00d), TransactionType.WITHDRAWAL,
                LocalDateTime.of(2019, 4, 8, 0, 0));
        Assertions.assertEquals(
                "Withdrawal, the 01/02/2019 of 20.00 euros. Your account balance is of -20.00 euros\n"
                        + "Deposit, the 02/02/2019 of 20.00 euros. Your account balance is of 0.00 euros\n"
                        + "Withdrawal, the 04/03/2019 of 5.00 euros. Your account balance is of -5.00 euros\n"
                        + "Deposit, the 06/04/2019 of 100.00 euros. Your account balance is of 95.00 euros\n"
                        + "Withdrawal, the 08/04/2019 of 35.00 euros. Your account balance is of 60.00 euros",
                printAccountTransactions(System.out, account));
    }

    @Test
    void printMultipleWithdrawalsCompensateDeposits() throws InvalidAmountException {
        Account account = new Account();
        updateAccountBalance(account, BigDecimal.valueOf(20.00d), TransactionType.WITHDRAWAL,
                LocalDateTime.of(2019, 2, 1, 0, 0));
        updateAccountBalance(account, BigDecimal.valueOf(20.00d), TransactionType.DEPOSIT,
                LocalDateTime.of(2019, 2, 2, 0, 0));
        updateAccountBalance(account, BigDecimal.valueOf(5.00d), TransactionType.WITHDRAWAL,
                LocalDateTime.of(2019, 3, 4, 0, 0));
        updateAccountBalance(account, BigDecimal.valueOf(100.00d), TransactionType.DEPOSIT,
                LocalDateTime.of(2019, 4, 6, 0, 0));
        updateAccountBalance(account, BigDecimal.valueOf(95.00d), TransactionType.WITHDRAWAL,
                LocalDateTime.of(2019, 4, 8, 0, 0));
        Assertions.assertEquals(
                "Withdrawal, the 01/02/2019 of 20.00 euros. Your account balance is of -20.00 euros\n"
                        + "Deposit, the 02/02/2019 of 20.00 euros. Your account balance is of 0.00 euros\n"
                        + "Withdrawal, the 04/03/2019 of 5.00 euros. Your account balance is of -5.00 euros\n"
                        + "Deposit, the 06/04/2019 of 100.00 euros. Your account balance is of 95.00 euros\n"
                        + "Withdrawal, the 08/04/2019 of 95.00 euros. Your account balance is of 0.00 euros",
                printAccountTransactions(System.out, account));
    }
}
